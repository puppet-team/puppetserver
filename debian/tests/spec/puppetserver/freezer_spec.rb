require 'spec_helper'
require 'fileutils'
require 'timeout'

describe "doesn't freeze on particular file resource in non-english locale" do
  before(:all) do
    # set up test puppet environment
    FileUtils.mkdir_p '/etc/puppet/code/environments/production'
    FileUtils.cp_r 'spec/fixtures/freezer', '/etc/puppet/code/environments'

    # switch to non-english default locale
    File.write('/etc/locale.conf', "LANG=de_CH.UTF-8\n")
    File.write('/etc/locale.gen', "de_CH UTF-8\n")
    system('locale-gen')

    # restart puppetserver
    puts 'restarting Puppet Server...'
    system('systemctl daemon-reexec')
    system('systemctl restart puppetserver')
  end

  around(:each) do |agent|
    Timeout::timeout(120) {
      agent.run
    }
  end

  describe command('puppet agent --test --environment=freezer') do
    its(:exit_status) { should eq 2 }
  end
end
