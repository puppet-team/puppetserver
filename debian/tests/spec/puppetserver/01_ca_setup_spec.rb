require 'spec_helper'

describe "/etc/puppetserver/ca should not exist on installation" do
  describe file('/etc/puppetserver/ca') do
    it { should_not exist }
  end
end

describe "puppetserver ca setup generates certificate authority" do
  describe command('puppetserver ca setup') do
    its(:exit_status) { should eq 0 }
    its(:stdout) { should contain 'Generation succeeded. Find your files in /etc/puppet/puppetserver/ca' }
  end
end

describe "puppetserver ca directory looks correct" do
  describe file('/etc/puppet/puppetserver/ca') do
    it { should be_directory }
    it { should be_owned_by 'puppet' }
  end

  describe file('/etc/puppet/puppetserver/ca/ca_key.pem') do
    it { should be_file }
    it { should be_mode 640 }
    its(:content) { should contain '-----BEGIN RSA PRIVATE KEY-----' }
  end

  describe file('/etc/puppet/puppetserver/ca/ca_pub.pem') do
    it { should be_file }
    it { should be_mode 640 }
    its(:content) { should contain '-----BEGIN PUBLIC KEY-----' }
  end

  describe file('/etc/puppet/puppetserver/ca/ca_crt.pem') do
    it { should be_file }
    it { should be_mode 640 }
    its(:content) { should contain '-----BEGIN CERTIFICATE-----' }
  end
end

describe "puppetserver agent certificate has the expected subject" do
  describe command("openssl x509 -noout -subject -in /etc/puppet/puppetserver/ca/ca_crt.pem | grep -Po '[^ ]+$' | tr -d '\\n'") do
    its(:stdout) { should eq 'puppet' }
  end
end
