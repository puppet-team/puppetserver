require 'spec_helper'

describe "naive autosign works" do
  describe command('puppet config set --section server autosign true') do
    its(:exit_status) { should eq 0 }
  end

  describe command('systemctl reload puppetserver') do
    its(:exit_status) { should eq 0 }
  end

  describe command('puppet ssl bootstrap --target agent.example.com --waitforcert 0') do
    its(:exit_status) { should eq 0 }
    its(:stdout) { should contain('Notice: Completed SSL initialization') }
  end

  describe command('puppetserver ca list --all') do
    its(:exit_status) { should eq 0 }
    its(:stdout) { should contain('DNS:agent.example.com') }
  end

  describe command('puppet agent --test --certname agent.example.com') do
    its(:exit_status) { should eq 0 }
    its(:stdout) { should contain('Caching catalog for agent.example.com') }
  end
end
