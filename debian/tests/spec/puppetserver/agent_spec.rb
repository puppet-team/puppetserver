require 'spec_helper'

describe "agent is able to retrieve empty catalog from server" do
  describe command('puppet agent --test') do
    its(:exit_status) { should eq 0 }
    its(:stdout) { should contain('Notice: Applied catalog') }
  end
end
