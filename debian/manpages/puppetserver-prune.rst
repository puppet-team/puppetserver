==================
puppetserver-prune
==================

---------------------------
Puppetserver cleanup helper
---------------------------

:Author: Jérôme Charaoui
:Date: 2024
:Manual section: 1

Synopsis
========

| ``puppetserver`` `prune` (--help)
| ``puppetserver`` `prune` `reportdir`\|\ `bucketdir` [`ttl`]

Description
===========

Deletes old files in `reportdir` or `bucketdir`. By default it deletes all files
older than 14 days.

The first argument must be reportdir or bucketdir, and the second (optional)
can be an `age` value that can be parsed by Puppet's `tidy` resource.

Bugs
====

Bugs can be reported to your distribution's bug tracker or upstream
at https://github.com/puppetlabs/puppetserver/issues

See Also
========

``puppetserver``\ (1), ``puppetserver-foreground``\ (1), ``puppetserver-gem``\ (1)
``puppetserver-ruby``\ (1), ``puppetserver-irb``\ (1), ``puppetserver-ca``\ (1),
